import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
// import DocPdf from '../../assets/document.pdf';
import DocPdf from '../../assets/doc2.docx';
const io = require("socket.io-client");
const axios = require('axios');

export function Tasks(props) {
  useEffect(() => {
    axios.get('http://localhost:8000/proofreading/get-nlp-key/').then((res) => {
      const token = res.data.nlp_key;
      console.log("TOKEN: ", token);
      const fileId = '2154820';

      var socket = io('https://nlp.scribendi.ai', { query: { token: token } });
      console.log('socket: ', socket);
      // Grammar correction websocket response handler
      socket.on('proof-response', function (data) {

        // There was an error
        if (!data.success) {
          console.log('Grammar correction ERROR!');
          console.table(data);
        } else {
          console.log('');
          console.log('Grammar correction response:');
          console.table(data);
        }
      });

      // const fileId = 'skjdhfkjs38923u3j23j324fdfsdf';
      // Classifier websocket response handler
      // console.log(socket.on('classifier-response-' + fileId, function (data) {

      //   // There was an error
      //   if (!data.success) {
      //     console.log('Classifier ERROR!');
      //     console.table(data);
      //   } else {
      //     console.log('');
      //     console.log('Classifier response:');
      //     console.table(data);
      //   }
      // }));


      // The classifier response is a little tricky.The response is actually a sequence of responses:



      socket.on('classifier-response-' + fileId, (data) => {
        let response = JSON.parse(data.response);

        if (data.success && response && response.status === 'received') {

          //received, I think this is what you, Mahedi, are printing out, the first response…

        } else if (data.success && response && response.status === 'parsed') {

          //parsed, look for word count in data

        } else if (data.success && response && response.status === 'complete' && data.message === 'Successfully processed the file.') {

          //complete, look for word count in data

        }

      });



      // You will first get a response with a status of received. Maybe this is what is being logged in your console.log example. Then you next get a response with the status of parsed. Lastly, a response with the status of complete. Please look at all these responses as one should have the word count.

      // Grammar correction request
      console.log('2 Emitting a grammar correction request...');
      console.log(socket.emit('proof', { model: 'model1', text: 'this are an test', token: token }));


      // Word count and classifier request
      console.log('Emitting a word count and classification request...');
      console.log(socket.emit('classifier', {
        file_id: fileId,
        file: DocPdf,
        s3: false,
        genre: true,
        token: token,
      }));
      socket.on("connect_error", (err) => {
        console.log(err);
        console.log(`connect_error due to ${JSON.stringify(err)}`);
      });

    });
  }, [])

  return (
    <div>
      <h4>All tasks</h4>
      <p>You will find all tasks here.</p>
      <span>
        <h4 style={{ margin: 0, display: "inline-block" }}>Here is your all tasks:</h4>
        <Link to="tasks/add">
          <button className="button">Add new</button>
        </Link>
      </span>
      <ol>
        {props.tasks.map((value) =>
          <li key={value.id}>
            <Link to={`/task/${value.id}`}>
              <span>{value.title}</span>
            </Link>
            <span style={{ marginLeft: "20px" }}>{value.member_name}</span>
          </li>)}
      </ol>
    </div>
  );
}
const mapStateToProps = (state) => {
  state.tasks.forEach((element, index) => {
    let memberIndex = state.members.findIndex(x => x.id === element.member_id)
    state.tasks[index] = { ...state.tasks[index], member_name: state.members[memberIndex].name }
  });
  return {
    tasks: state.tasks
  }
}
export default connect(mapStateToProps)(Tasks);